Destination = class('Destination')

function Destination:initialize(grid_x, grid_y)
  self.grid_x = grid_x
  self.grid_y = grid_y
  self.x = self.grid_x*grid_ppNo
  self.y = self.grid_y*grid_ppNo
  self.width = grid_ppNo
  self.height = grid_ppNo
  table.insert(list_destination, self)
end

function Destination:draw()
  love.graphics.setColor(0.1,0.1,0.8)
  love.graphics.rectangle("fill", self.grid_x*grid_ppNo, self.grid_y*grid_ppNo, self.width, self.height)
end

return Destination