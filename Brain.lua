Brain = class('Brain')

function Brain:initialize(size)
  self.directions = {}
  self.size = size
end

function Brain:randomize(size)
  for i=0,size do
    direction = {}
    direction.x = (0.5 - math.random())
    direction.y = (0.5 - math.random())
    self.directions[i] = direction
  end
end

function Brain:copy(brainToCopy) -- Kopiere ein existierendes Hirn in ein neues -> d.h. nicht nur den Zeiger auf das existierende, sonst ist mutate immer nur auf dem existierenden
  for i=0, brainToCopy.size do
    self.directions[i] = {}
      self.directions[i].x = brainToCopy.directions[i].x
      self.directions[i].y = brainToCopy.directions[i].y
  end
  self.size = brainToCopy.size
end

function Brain:mutate(incidence, intensity) -- Parameter in Prozent
  for i=0, self.size do
    if (math.random() < incidence/100) then
      self.directions[i].x = self.directions[i].x + (0.5 - math.random())*(intensity/100)
      self.directions[i].y = self.directions[i].y + (0.5 - math.random())*(intensity/100)
    end
  end
end

function Brain:addNewBrainsteps(numbersOfSteps)
  for i=1,numbersOfSteps do
    direction = {}
    direction.x = (0.5 - math.random())
    direction.y = (0.5 - math.random())
    self.size = self.size + 1
    table.insert(self.directions, direction)
  end
end

-- löscht alle steps nach dem angegebenen. Falls der dude vorzeitig ablebt, soll er die steps danach nicht auch vererben
function Brain:removeStepsAfter(stepNumber)
  local positionsToRemove = #self.directions - stepNumber
  for i=1,positionsToRemove do
    table.remove(self.directions)
    self.size = self.size - 1
  end
end


return Brain