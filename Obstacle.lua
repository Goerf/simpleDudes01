Obstacle = class('Obstacle')

function Obstacle:initialize(grid_x, grid_y, width) -- grid_x und y sowie width in Grid-Nr.-Position und nicht Pixel!
  self.grid_x = grid_x
  self.grid_y = grid_y
  self.x = self.grid_x*grid_ppNo
  self.y = self.grid_y*grid_ppNo
  self.width = width*grid_ppNo
  self.height = grid_ppNo
  self.isborder = false -- wenn der flag gesetzt ist, vereinfache die Kollisionsabfrage
  if(self.grid_x <= 0 or self.grid_x >= grid_no_longitude-1) then
    self.isborder = true
  end
  if(self.grid_y <= 0 or self.grid_y >= grid_no_longitude-1) then
    self.isborder = true
  end
  table.insert(list_obstacles, self)
end

function Obstacle:draw()
  love.graphics.setColor(0.9,0.1,0.1)
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
  love.graphics.setColor(1,1,1)
  love.graphics.print(tostring(self.grid_x).."/"..tostring(self.grid_y).."/w: "..self.width, self.x, self.y)
end

return Obstacle