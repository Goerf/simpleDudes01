Label = class('Label')


function Label:initialize(x, y, text) -- x und y in realen pixel-Koordinaten (genaue, feine Bewegung ohne Grid)
  self.x = x
  self.y = y
  self.text = text
  self.generation = generation
  self.color = {}
  self.color["r"] = 1
  self.color["g"] = 1
  self.color["b"] = 1
  table.insert(list_labels, self)
end

function Label:draw()
  love.graphics.setColor(self.color["r"],self.color["g"],self.color["b"])
  love.graphics.print(tostring(self.text), self.x, self.y)
end

function Label:update(dt)
  if(generation >= self.generation + 1) then
    self.color["r"] = 0.5
    self.color["g"] = 0.5
    self.color["b"] = 0.5
  end
  if(generation >= self.generation + 2) then
    self.color["r"] = 0.1
    self.color["g"] = 0.1
    self.color["b"] = 0.1
  end
  if(generation > self.generation + MaxgenerationsOfLabels) then
    local index = nil
    for i, v in pairs(list_labels) do
      if v == self then
        index = i
      end
    end
    if index ~= nil then
      table.remove(list_labels, index)
    else
      print("label not found in the list_labels!!")
    end
  end
end

return Label