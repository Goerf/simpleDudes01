Spawn = class('Spawn')

function Spawn:initialize(grid_x, grid_y)
  self.grid_x = grid_x
  self.grid_y = grid_y
  self.x = self.grid_x*grid_ppNo
  self.y = self.grid_y*grid_ppNo
  print("Spawn-Pos: ", self.grid_x, self.grid_y)
  self.width = grid_ppNo
  self.height = grid_ppNo
  table.insert(list_spawn, self)
end

function Spawn:draw()
  love.graphics.setColor(0.1,0.8,0.1)
  love.graphics.rectangle("fill", self.grid_x*grid_ppNo, self.grid_y*grid_ppNo, self.width, self.height)
end

return Obstacle