Dude = class('Dude')

function Dude:initialize(x, y) -- x und y in realen pixel-Koordinaten (genaue, feine Bewegung ohne Grid)
  self.x = x
  self.y = y
  self.speed = 400
  self.health = 100
  self.vx = 0
  self.vy = 0
  self.brain = nil
  self.width = grid_ppNo/2
  self.height = grid_ppNo/2
  self.tripmeter = 0
  self.brainstep = 0
  self.fitness = 0
  table.insert(list_dudes, self)
end

function Dude:draw()
  if(self.health > 0) then
    love.graphics.setColor(0.1,0.95,0.3)
  else
    love.graphics.setColor(0.4,0.4,0.4)
  end
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
  --love.graphics.points(self.x, self.y)
end

function Dude:update(dt)
  if(self.health > 0 and self.brain ~= nil) then
    self.tripmeter = self.tripmeter + self.speed/150 * dt
    if(self.brainstep >= self.brain.size) then
      self:die()
      return
    end
    if(self.tripmeter >= 1) then
      self.tripmeter = 0
      --print("self.brainstep :"..self.brainstep)
      self.vx = self.brain.directions[self.brainstep].x
      self.vy = self.brain.directions[self.brainstep].y
      self.brainstep = self.brainstep + 1
    end
    self.x = self.x + self.vx * self.speed * dt
    self.y = self.y + self.vy * self.speed * dt
    -- Kollisionserkennung
    --   vereinfachte Erkennung: Rahmen
    if (self.x <= grid_ppNo or self.x >= love.graphics.getWidth() - grid_ppNo or self.y <= grid_ppNo or self.y >= love.graphics.getHeight() - grid_ppNo) then
      self:die()
    end
    --   Detailerkennung: Mauern im Spielfeld
    for i,v in ipairs(list_obstacles) do
      if(v.isborder == false) then
        --if (math.abs(self.x-v.x) <  grid_ppNo and math.abs(self.y-v.y) <  grid_ppNo) then
        if (self:checkCollision(self.x,self.y,self.width,self.height, v.x,v.y,v.width,v.height)) then
          self:die()
        end
      end
    end
    for i,v in ipairs(list_destination) do
      if (math.abs(self.x-v.x) <  grid_ppNo and math.abs(self.y-v.y) <  grid_ppNo) then
        self:die()
      end
    end
  end
end

function Dude:die()
  self.health = 0
  -- lösche alle nicht verwendeten steps, da die nicht weitervererbt werden dürfen und logisch können
  --print("--\nself.brainstep: "..self.brainstep)
  --print("#self.brain.directions: "..#self.brain.directions)
  self.brain:removeStepsAfter(self.brainstep)
  --print("#self.brain.directions: "..#self.brain.directions)
end

-- Collision detection function;
-- Returns true if two boxes overlap, false if they don't;
-- x1,y1 are the top-left coords of the first box, while w1,h1 are its width and height;
-- x2,y2,w2 & h2 are the same, but for the second box.
function Dude:checkCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function Dude:getFitness()
  for i, v in pairs(list_destination) do
    self.fitness = math.floor(math.pow(math.pow((self.x - v.x),2) + math.pow((self.y - v.y),2), 0.5)) + self.brainstep -- Fitness ist Distanz zum Ziel + Anzahl an Schritten
  end
end

function Dude:delete() -- deletes dude and his brain
  --table.remove(list_dudes, self)
  --find position of the element in the list
  index= nil
  for i, v in pairs(list_dudes) do
    if v == self then
      index = i
    end
  end
  if index ~= nil then
    table.remove(list_dudes, index)
  else
    print("dude not found in the list_dudes!!")
  end
end

function Dude:getRandomBrain(size)
  self.brain = Brain:new(size)
  self.brain:randomize(size)
end
function Dude:mutateBrain(incidence, intensity)
  self.brain:mutate(incidence, intensity)
end
function Dude:copyBrain(brainToCopy)
  self.brain = Brain:new(brainToCopy.size)
  self.brain:copy(brainToCopy)
end

function Dude:deleteLastMadeBrainsteps(numbersOfSteps)
  for i=1, numbersOfSteps do
    self.brain.directions[#self.brain.directions] = nil -- removes the last entry
    self.brain.size = self.brain.size - 1
  end
end

function Dude:addNewBrainsteps(numbersOfSteps)
  self.brain:addNewBrainsteps(numbersOfSteps)
end

return Dude