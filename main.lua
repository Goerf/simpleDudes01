
class = require 'middleclass'
require("Obstacle")
require("Destination")
require("Spawn")
require("Brain")
require("Dude")
require("Label")

list_dudes = {}
list_obstacles = {}
list_spawn = {}
list_destination = {}
list_labels = {}
MaxgenerationsOfLabels = 0

grid_ppNo = 20
grid_no_longitude = 0
num_dudes =150
num_dudesAlive = 0
generation = 0

obstacleStart = false
function love.load(arg)
  --if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
  math.randomseed(os.time())
  local grid_no_x = 0
  local grid_no_y = 0
  -- überprüfe die Größe der (quadratischen!) Karte
  for line in love.filesystem.lines("map04.csv") do
    grid_no_longitude = grid_no_longitude + 1
  end
  print("size of map: "..grid_no_longitude)
  -- create the map
  for line in love.filesystem.lines("map04.csv") do
    grid_no_x = 0
    for csValue in (line .. ","):gmatch("([^,]*),") do 
      --print(grid_no_x, csValue, obstacleStart)
      if csValue == "E" and obstacleStart ~= false then
        Obstacle:new(obstacleStart, grid_no_y, math.abs(obstacleStart-grid_no_x))
        print("obstacle end at: "..grid_no_x)
        obstacleStart = false
      end
      if csValue == "S" and obstacleStart == false then
        obstacleStart = grid_no_x
        print("obstacle start at: "..grid_no_x)
      end
      if csValue == "2" then
        Spawn:new(grid_no_x, grid_no_y)
      end
      if csValue == "3" then
        Destination:new(grid_no_x, grid_no_y)
      end
      grid_no_x = grid_no_x + 1
    end
    grid_no_y = grid_no_y + 1
  end
end

start = 0
function love.keyreleased(key)
  -- in v0.9.2 and earlier space is represented by the actual space character ' ', so check for both
  if (key == " " or key == "space") then
    for i=0,num_dudes do
      for j, v in ipairs(list_spawn) do
        local dude = Dude:new(v.x,v.y)
        dude:getRandomBrain(10)
      end
    end
    start = 1
  end
end

function love.update(dt)
  if start == 1 then
    -- update those dudes
    num_dudesAlive = 0
    for i,v in pairs(list_dudes) do
      v:update(dt)
      if v.health > 0 then num_dudesAlive = num_dudesAlive + 1 end
    end
    if (num_dudesAlive < num_dudes * 0.1) then
      generation = generation + 1
      local bestFitness = 11000
      local fittestBrain = 0
      for i, v in pairs(list_dudes) do
        v:getFitness()
        Label:new(v.x, v.y, v.fitness)
        if v.fitness < bestFitness then
          fittestBrain = v.brain
          bestFitness = v.fitness
        end
      end
      for i, value in ipairs(fittestBrain.directions) do
        --print(i, value.x, value.y)
      end
      -- nehme den Besten, lösche die anderen
      --for i, v in pairs(list_dudes) do
      --  v:delete()
      --end
      list_dudes = {}
      --math.randomseed(os.time())
      --print("--------------------------------------------")
      for i=0,num_dudes do
        for j, v in ipairs(list_spawn) do
          local dude = Dude:new(v.x,v.y)
          dude:copyBrain(fittestBrain)
          dude:mutateBrain(100,40) -- X % der Steps mutieren zu einer Intensität von Y % der maximal möglichen Intentsität
          --print("----------------------\nDude-report of new dude: brainsteps before remove: "..#dude.brain.directions)
          dude:deleteLastMadeBrainsteps(2)
          --print(" brainsteps after remove: "..#dude.brain.directions)
          if (bestFitness > 10) then
            dude:addNewBrainsteps(10)
            --print(" brainsteps after adding: "..#dude.brain.directions)
          end
        end
      end
      --print("--------------------------------------------")
    end
  end
  for i, v in pairs(list_labels) do
    v:update(dt)
  end
end

function love.draw()
  -- let's draw a background
  love.graphics.setColor(0.9,0.1,0.1)
  love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  love.graphics.setColor(0,0,0)
  love.graphics.rectangle("fill", grid_ppNo, grid_ppNo, love.graphics.getWidth()-grid_ppNo*2, love.graphics.getHeight()-grid_ppNo*2)
  
  -- let's draw our destination
  for i,v in pairs(list_destination) do
    v:draw()
  end
  -- let's draw our spawns
  for i,v in pairs(list_spawn) do
    v:draw()
  end
  -- let's draw our obstacles
  for i,v in pairs(list_obstacles) do
    v:draw()
  end
   --lets draw the dudes
  for i,v in pairs(list_dudes) do
    v:draw()
  end
  for i,v in pairs(list_labels) do
    v:draw()
  end
  love.graphics.setColor(1,1,1)
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS()), 10, 10)
  love.graphics.print("Current alive: "..tostring(num_dudesAlive), 200, 10)
  love.graphics.print("Current generation: "..tostring(generation), 200, 50)
end